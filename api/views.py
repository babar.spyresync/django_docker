from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.tokens import AccessToken
from .serializers import *
from .models import *


# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [AllowAny]

    def signup(self, request, *args, **kwargs):
        try:
            serializer = UserSignupSerializer(data=request.data)
            try:
                serializer.is_valid(raise_exception=True)
            except Exception as e:
                error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                         "errors": e.args[0]}
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
            serializer.save()
            user = User.objects.get(email=serializer.data['email'])
            access_token = AccessToken.for_user(user)
            response = {
                "statusCode": 201,
                "error": False,
                "message": "User created successfully",
                "data": {
                    "user": UserSerializer(user).data,
                    "access_token": str(access_token)
                }}
            return Response(response, status=status.HTTP_201_CREATED)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

    def login(self, request, *args, **kwargs):
        try:
            serializer = UserLoginSerializer(data=request.data)
            try:
                serializer.is_valid(raise_exception=True)
            except Exception as e:
                error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                         "errors": e.args[0]}
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
            user = User.objects.get(email=serializer.data['email'])
            access_token = AccessToken.for_user(user)
            response = {
                "statusCode": 200,
                "error": False,
                "message": "User logged in successfully",
                "data": {
                    "user": UserSerializer(user).data,
                    "access_token": str(access_token)
                }}
            return Response(response, status=status.HTTP_200_OK)
        except Exception as e:
            error = {"statusCode": 400, "error": True, "data": "", "message": "Bad Request, Please check request",
                     "errors": e.args[0]}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

