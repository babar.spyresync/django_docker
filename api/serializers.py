from django.contrib.auth.hashers import make_password
from django.db import transaction
from .models import User
from rest_framework import serializers


class UserSignupSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=50, required=True)
    email = serializers.EmailField(required=True)
    password = serializers.CharField(max_length=128, required=True, write_only=True)

    def validate(self, data):
        email = User.objects.filter(email=data['email'])
        if email:
            raise serializers.ValidationError("Email already exists")
        username = User.objects.filter(username=data['username'])
        if username:
            raise serializers.ValidationError("Username already exists")
        return data

    def create(self, validated_data):
        with transaction.atomic():
            user = User.objects.create(
                username=validated_data['username'],
                email=validated_data['email'],
                password=make_password(validated_data['password'])
            )
            return user


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(max_length=128, required=True, write_only=True)

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        if email and password:
            user = User.objects.filter(email=email).first()
            if user and user.check_password(password):
                return user
            else:
                raise serializers.ValidationError("Invalid Credentials")
        else:
            raise serializers.ValidationError("Please enter email and password")


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'phone', 'address')
