django==3.2.12
djangorestframework==3.13.1
djangorestframework-simplejwt==4.3.0
django-cors-headers==3.10.1
PyJWT==1.7.1